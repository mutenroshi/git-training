package helloworld;

import javax.swing.JOptionPane;

public class HelloRemoteGit {
	public static void main(String[] args) {
		String repositorio = JOptionPane.showInputDialog("Introduce repositorio");
		if(repositorio.equalsIgnoreCase("gitlab"))
			System.out.println("Hello GitLab");
		else if(repositorio.equalsIgnoreCase("github"))
			System.out.println("Hello GitHub");
	}
}
