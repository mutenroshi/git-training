package helloworld;

import javax.swing.JOptionPane;

public class HelloWorld {

	public static void main(String[] args) {
		
		String nombre = JOptionPane.showInputDialog("Introduce tu nombre");
		printHola(nombre);
	}

	public static void printHola(String nombre) {
		if(nombre != null && !nombre.equals(""))
			System.out.println("Hola " + nombre);
		else
			System.out.println("Hola mundo!");
	}
}
